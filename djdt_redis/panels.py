import itertools
import time

from functools import wraps
from weakref import WeakKeyDictionary

from redis import StrictRedis as Redis

try:
    from redis.client import BasePipeline as Pipeline
except ImportError:
    from redis.client import Pipeline

from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _, ngettext_lazy as __

from debug_toolbar import settings as dt_settings
from debug_toolbar.panels import Panel
from debug_toolbar.utils import render_stacktrace
try:
    from debug_toolbar.utils import get_stack_trace
except ImportError:
    from djdt_redis.compat import get_stack_trace


def memoized_property(method):
    SENTINEL = object()
    cache = WeakKeyDictionary()

    @property
    @wraps(method)
    def wrapper(self):
        result = cache.get(self, SENTINEL)
        if result is SENTINEL:
            result = method(self)
            cache[self] = result
        return result

    return wrapper


def _utf8_char_len(c):
    i = ord(c)
    if i <= 0x7F:
        return 1
    elif i <= 0x7FF:
        return 2
    elif i <= 0xFFFF:
        return 3
    else:
        return 4


def _utf8_len(s):
    return sum(_utf8_char_len(c) for c in s)


def _arg_size(arg):
    if isinstance(arg, bytes):
        return len(arg)
    elif isinstance(arg, (int, float)):
        return len(repr(arg))
    elif isinstance(arg, str):
        return _utf8_len(arg)
    else:
        raise Exception("Unknown arg type '{}'".format(type(arg)))


def _result_size(result):
    if result is None:
        return 0
    elif isinstance(result, bool):
        return 1
    elif isinstance(result, (int, float)):
        return len(repr(result))
    elif isinstance(result, bytes):
        return len(result)
    elif isinstance(result, dict):
        return sum(
            _result_size(key) + _result_size(value)
            for key, value in result.items()
        )
    elif isinstance(result, (list, tuple)):
        return sum(_result_size(value) for value in result)
    else:
        raise Exception("Unknown result type '{}'".format(type(result)))


def arg_to_str(arg):
    if isinstance(arg, bytes):
        return '<{} byte{}>'.format(len(arg), 's' if len(arg) > 1 else '')
    else:
        return str(arg)


class _Call:
    def __init__(
        self,
        stacktrace,
        ops,
        duration,
        exception,
        is_pipeline=False,
    ):
        self.stacktrace = stacktrace
        self.ops = ops
        self.duration = duration
        self.exception = exception
        self.is_pipeline = is_pipeline

    @property
    def args_size(self):
        return sum(op.args_size for op in self.ops)

    @property
    def results_size(self):
        return sum(op.result_size for op in self.ops)


class _Operation:
    def __init__(self, args, result):
        self.command = args[0]
        self.args = args[1:]
        self.result = result

    def args_str(self):
        n = 0
        parts = []
        for arg in self.args:
            arg_str = arg_to_str(arg)
            if parts:
                n += 1
            n += len(arg_str)
            if n >= 255:
                parts.append('... [{} additional args]'.format(len(self.args) - len(parts)))
                break
            else:
                parts.append(arg_str)
        return ' '.join(parts)

    @memoized_property
    def args_size(self):
        return sum(_arg_size(arg) for arg in self.args)

    @memoized_property
    def result_size(self):
        return _result_size(self.result)


def _wrap_execute_command(panel, orig_execute_command):
    def wrapper(self, *args, **kwargs):
        stacktrace = panel.get_stacktrace()
        exception = None
        start = time.time()
        try:
            result = orig_execute_command(self, *args, **kwargs)
        except Exception as e:
            exception = e
            result = None
            raise
        else:
            return result
        finally:
            stop = time.time()
            duration = (stop - start) * 1000
            panel.calls.append(_Call(
                stacktrace,
                [_Operation(args, result)],
                duration,
                exception,
            ))
            panel.call_time += duration

    return wrapper


def _wrap_pipeline_execute(panel, orig_pipeline_execute):
    def wrapper(self, *args, **kwargs):
        stacktrace = panel.get_stacktrace()
        exception = None
        command_stack = self.command_stack.copy()
        start = time.time()
        try:
            results = orig_pipeline_execute(self, *args, **kwargs)
        except Exception as e:
            exception = e
            results = itertools.repeat(None)
            raise
        else:
            return results
        finally:
            stop = time.time()
            command_args_iter = (
                command_args for command_args, _ in command_stack
            )
            ops = [
                _Operation(command_args, result)
                for command_args, result in zip(command_args_iter, results)
            ]
            duration = (stop - start) * 1000
            panel.calls.append(_Call(
                stacktrace,
                ops,
                duration,
                exception,
                is_pipeline=True,
            ))
            panel.call_time += duration

    return wrapper


class _Stacktrace:
    def __init__(self):
        self.stacktrace = [
            frame for frame in get_stack_trace() if frame[0] != __file__
        ]
        self.rendered = None

    def render(self):
        if self.rendered is None:
            self.rendered = render_stacktrace(self.stacktrace)
        return self.rendered


class RedisPanel(Panel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.enable_stacktraces = dt_settings.get_config()["ENABLE_STACKTRACES"]

        self.calls = []
        self.call_time = 0

        self.orig_execute_command = Redis.execute_command
        self.orig_pipeline_execute = Pipeline.execute

        self.wrapped_execute_command = _wrap_execute_command(
            self, Redis.execute_command
        )
        self.wrapped_pipeline_execute = _wrap_pipeline_execute(
            self, Pipeline.execute
        )

    title = _("Redis")

    @property
    def nav_subtitle(self):
        return __(
            "{} call in {:0.2f}ms", "{} calls in {:0.2f}ms", len(self.calls)
        ).format(len(self.calls), self.call_time)

    @property
    def has_content(self):
        return bool(self.calls)

    @property
    def content(self):
        context = {
            "enable_stacktraces": self.enable_stacktraces,
            "calls": self.calls,
        }
        return render_to_string("djdt_redis/panels/redis.html", context)

    def enable_instrumentation(self):
        Redis.execute_command = self.wrapped_execute_command
        Pipeline.execute = self.wrapped_pipeline_execute

    def disable_instrumentation(self):
        Redis.execute_command = self.orig_execute_command
        Pipeline.execute = self.orig_pipeline_execute

    def get_stacktrace(self):
        if self.enable_stacktraces:
            return _Stacktrace()
        else:
            return []
