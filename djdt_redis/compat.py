from debug_toolbar import settings as dt_settings
from debug_toolbar.utils import get_stack, tidy_stacktrace


def get_stack_trace():
    if not dt_settings.get_config()["ENABLE_STACKTRACES"]:
        return []
    return tidy_stacktrace(reversed(get_stack()))
