#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='djdt-redis',
    version='0.1.0',
    author='Daniel Harding',
    author_email='dharding@living180.net',
    description='A Redis panel for Django Debug Toolbar',
    license="MIT",
    url='',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],
    packages=find_packages(),
    python_requires='>=3.5',
    install_requires=['django-debug-toolbar>=1.0'],
    include_package_data=True,
    zip_safe=False,
)
